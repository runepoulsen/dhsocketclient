from setuptools import setup

setup(
    name='dhsocketclient',
    version='0.0.1',
    description='My private package from private github repo',
    url='https://runepoulsen@bitbucket.org/runepoulsen/dhsocketclient.git',
    author='Rune',
    author_email='runesp@gmail.com',
    license='unlicense',
    packages=['dhsocketclient'],
    zip_safe=False,
    install_requires=['python-socketio', 'rx']
)
