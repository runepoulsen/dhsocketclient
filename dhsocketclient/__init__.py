import socketio
import eventlet
from rx.subject import BehaviorSubject
from rx import operators as ops


class DhSocketClient:

    def __init__(self, host, namespaces=None, debug=False, binary=True):
        self.debug = debug
        self.sio = None
        self.connected = False
        self.connection_sub = BehaviorSubject(False)
        self.room_sub = None
        self.host = host
        self.namespaces = namespaces or []
        self.sio = socketio.Client(binary=binary,
                                   logger=debug,
                                   engineio_logger=debug)
        self.sio.on('connect', self.on_connect)
        self.sio.on('disconnect', self.on_disconnect)
        self.sio.on('message', self.on_message)
        self.watchdog_thread = None

    def start(self):
        if self.watchdog_thread is None:
            # print('creating watchdog')
            self.watchdog_thread = eventlet.spawn(self.watchdog_process)

    def watchdog_process(self):
        while True:
            if not self.connected:
                self.connect()
            eventlet.sleep(5)

    def connect(self):
        if not self.host:
            print('No host specified')
            return
        try:
            if self.debug:
                print('Attempting to connect')
            self.sio.connect(self.host, namespaces=self.namespaces)
        except socketio.exceptions.ConnectionError as ex:
            print(f"Unable to connect to {self.host}: {ex}")
            self.connected = False

    def on_connect(self):
        self.connected = True
        self.connection_sub.on_next(True)
        if self.debug:
            print(f'Connected to: {self.host}')

    def on_disconnect(self):
        self.connected = False
        self.connection_sub.on_next(False)
        if self.debug:
            print(f'Disconnected from: {self.host}')

    def on_message(self, data):  # overwrite this
        pass

    def close(self):
        if self.debug:
            print(f'Closing {self.host}')
        if self.sio:
            try:
                self.sio.disconnect()
                self.on_disconnect()
            except Exception as ex:
                print(f'Failed to disconnect client {str(ex)}')

        if self.watchdog_thread is not None:
            self.watchdog_thread.kill()
            self.watchdog_thread = None

    def enter_room(self, room):
        if self.sio is None: return
        self.room_sub = self.connection_sub.pipe(
            ops.filter(lambda val: val is True),
        ).subscribe(lambda value: self.sio.emit('enter_room', room))

    def leave_room(self, room):
        if not self.connected or self.sio is None: return
        if self.room_sub: self.room_sub.dispose()
        self.sio.emit('leave_room', room)
